# Uncomment to get zsh profiling.
# Don't forget to uncomment the closing zprof in the last line of this file
# zmodload zsh/zprof

export ZSH=$HOME/.oh-my-zsh

# Do not ask twice if I really want to close the current terminal with running jobs
setopt NO_HUP
# Stop warning about running jobs
setopt NO_CHECK_JOBS
# stepping through your history with UP and DOWN keys will not show duplicates
setopt HIST_FIND_NO_DUPS
# prevent duplicates from being saved in your history file
setopt HIST_IGNORE_ALL_DUPS

ZSH_THEME="ys"
HYPHEN_INSENSITIVE="true"
COMPLETION_WAITING_DOTS="true"

# This may cause errors with other terminal commands which depend on this delay (issues -> raise the delay)
export KEYTIMEOUT=1 # 1 is a delay of 0.1 seconds

HISTFILE=~/.zsh_history
HISTSIZE=999999999
SAVEHIST=$HISTSIZE
HIST_STAMPS="yyyy-mm-dd"
setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
setopt HIST_BEEP                 # Beep when accessing nonexistent history.

export UPDATE_ZSH_DAYS=7

plugins=(
  gpg-agent
  ssh-agent
  taskwarrior
  vi-mode
  git
  gitfast
  colored-man-pages
  web-search
  docker
  pip
  pass
  kubectl
  zsh-autosuggestions #manual git clone necessary
  fast-syntax-highlighting #manual git clone necessary
  # Load after zsh-autosuggestions and fast-syntax-highlighting
  fzf-tab #manual git clone necessary
)

# zsh-autosuggestions configuration
bindkey '^j' autosuggest-accept

zstyle :omz:plugins:ssh-agent agent-forwarding on
zstyle :omz:plugins:ssh-agent identities id_rsa
source $ZSH/oh-my-zsh.sh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f ~/.functions ] && source ~/.functions
[ -f ~/.aliases ] && source ~/.aliases
[ -f ~/.xrandr ] && source ~/.xrandr
[ -f ~/.taskwarrior ] && source ~/.taskwarrior

eval "$(mise activate zsh)"

# The following lines were added by compinstall
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' completer _complete _ignored
zstyle :compinstall filename '$HOME/.zshrc'
# End of lines added by compinstall

# Auto completion stuff
zstyle ':completion::complete:ccpass::' prefix "$HOME/projects/password-store"

# Recommendations by fzf-tab
# disable sort when completing `git checkout`
zstyle ':completion:*:git-checkout:*' sort false
# set descriptions format to enable group support
zstyle ':completion:*:descriptions' format '[%d]'
# set list-colors to enable filename colorizing
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
# preview directory's content with exa when completing cd
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'exa -1 --color=always $realpath'
# switch group using `,` and `.`
zstyle ':fzf-tab:*' switch-group ',' '.'

# make pasting to terminal fast 
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic
zstyle ':bracketed-paste-magic' active-widgets '.self-*'

# The next line updates PATH for the Google Cloud SDK.
if [ -f '$HOME/google-cloud-sdk/path.zsh.inc' ]; then . '$HOME/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '$HOME/google-cloud-sdk/completion.zsh.inc' ]; then . '$HOME/google-cloud-sdk/completion.zsh.inc'; fi

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# zprof
