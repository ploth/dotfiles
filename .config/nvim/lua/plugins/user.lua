return {
  {
    "mg979/vim-visual-multi",
    lazy = false,
  },
  {
    "tpope/vim-fugitive",
    lazy = false,
  },
  {
    "harrisoncramer/gitlab.nvim",
    dependencies = {
      "MunifTanjim/nui.nvim",
      "nvim-lua/plenary.nvim",
      "sindrets/diffview.nvim",
      "stevearc/dressing.nvim", -- Recommended but not required. Better UI for pickers.
      "nvim-tree/nvim-web-devicons", -- Recommended but not required. Icons in discussion tree.
    },
    enabled = true,
    build = function() require("gitlab.server").build(true) end, -- Builds the Go binary
    config = function()
      local gitlab = require "gitlab"
      local gitlab_server = require "gitlab.server"
      gitlab.setup()
      vim.keymap.set("n", "gLb", gitlab.choose_merge_request)
      vim.keymap.set("n", "gLr", gitlab.review)
      vim.keymap.set("n", "gLs", gitlab.summary)
      vim.keymap.set("n", "gLA", gitlab.approve)
      vim.keymap.set("n", "gLR", gitlab.revoke)
      vim.keymap.set("n", "gLc", gitlab.create_comment)
      vim.keymap.set("v", "gLc", gitlab.create_multiline_comment)
      vim.keymap.set("v", "gLC", gitlab.create_comment_suggestion)
      vim.keymap.set("n", "gLO", gitlab.create_mr)
      vim.keymap.set("n", "gLm", gitlab.move_to_discussion_tree_from_diagnostic)
      vim.keymap.set("n", "gLn", gitlab.create_note)
      vim.keymap.set("n", "gLd", gitlab.toggle_discussions)
      vim.keymap.set("n", "gLaa", gitlab.add_assignee)
      vim.keymap.set("n", "gLad", gitlab.delete_assignee)
      vim.keymap.set("n", "gLla", gitlab.add_label)
      vim.keymap.set("n", "gLld", gitlab.delete_label)
      vim.keymap.set("n", "gLra", gitlab.add_reviewer)
      vim.keymap.set("n", "gLrd", gitlab.delete_reviewer)
      vim.keymap.set("n", "gLp", gitlab.pipeline)
      vim.keymap.set("n", "gLo", gitlab.open_in_browser)
      vim.keymap.set("n", "gLM", gitlab.merge)
      vim.keymap.set("n", "gLu", gitlab.copy_mr_url)
      vim.keymap.set("n", "gLP", gitlab.publish_all_drafts)
      vim.keymap.set("n", "gLD", gitlab.toggle_draft_mode)
    end,
  },
}
