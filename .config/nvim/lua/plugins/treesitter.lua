-- Customize Treesitter

---@type LazySpec
return {
  "nvim-treesitter/nvim-treesitter",
  opts = function(_, opts)
    -- add more things to the ensure_installed table protecting against community packs modifying it
    opts.ensure_installed = require("astrocore").list_insert_unique(opts.ensure_installed, {
      "astro",
      "bash",
      "c_sharp",
      "comment",
      "css",
      "csv",
      "dart",
      "diff",
      "dockerfile",
      "git_config",
      "git_rebase",
      "gitattributes",
      "gitcommit",
      "gitignore",
      "go",
      "gpg",
      "helm",
      "html",
      "http",
      "java",
      "javascript",
      "jq",
      "jsdoc",
      "json",
      "json5",
      "kotlin",
      "lua",
      "luadoc",
      "make",
      "prisma",
      "proto",
      "python",
      "regex",
      "scss",
      "sql",
      "ssh_config",
      "terraform",
      "toml",
      "tsx",
      "typescript",
      "vim",
      "vue",
      "xml",
      "yaml",
      -- add more arguments for adding more treesitter parsers
    })

    -- disable python treesitter parser (Strange indentation quirk when writing loops)
    opts.indent.disable = require("astrocore").list_insert_unique(opts.indent.disable, { "python" })
  end,
}
