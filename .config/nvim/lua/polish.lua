-- This will run last in the setup process and is a good place to configure
-- things like custom filetypes. This just pure lua so anything that doesn't
-- fit in the normal config locations above can go here

-- Set up custom filetypes
vim.filetype.add {
  extension = {
    foo = "fooscript",
  },
  filename = {
    ["Foofile"] = "fooscript",
  },
  pattern = {
    ["~/%.config/foo/.*"] = "fooscript",
  },
}

-- Keep search highlighting when scrolling
-- https://github.com/AstroNvim/AstroNvim/issues/1529#issuecomment-1379208751
vim.on_key(nil, vim.api.nvim_get_namespaces()["auto_hlsearch"])
